/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

#![doc(test(attr(allow(unused_variables), deny(warnings))))]

error_chain! {
    foreign_links {
    }
}


#[macro_use]
extern crate error_chain;

extern crate llang_lib;
extern crate clap;
extern crate rustyline;

use clap::{Arg, App};

fn get_filename() -> String {
    let matches = App::new("llang")
        .version("0.0.1")
        .author("Afonso Bordado <afonsobordado@az8.co")
        .about("LLVM based compiler for lua")
        .arg(Arg::with_name("file")
            .value_name("FILE")
            .help("File to be compiled")
            .required(true)
            .takes_value(true))
        .get_matches();

    matches.value_of("file")
        .expect("no filename provided")
        .into()
}


fn main() {

    let filename = get_filename();
    let ast = llang_lib::parse(filename).expect("Parse error");
    println!("{:?}", ast);
    
    llang_lib::run(ast);
}
