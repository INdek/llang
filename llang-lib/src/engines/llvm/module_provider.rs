/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use context::Context;
use llvm::{self, Module, Function};
use std::collections::HashMap;

pub trait ModuleProvider {
    fn dump(&self);
    fn get_module(&mut self) -> &mut Module;
    fn get_function(&mut self, name: &str) -> Option<(Function, bool)>;
}

pub struct SimpleModuleProvider {
    module: Module,
}

impl SimpleModuleProvider {
    pub fn new(context: &mut Context, name: &str) -> SimpleModuleProvider {

        SimpleModuleProvider { module: context.context.module_create_with_name(name) }
    }
}

impl ModuleProvider for SimpleModuleProvider {
    fn dump(&self) {
        self.module.dump();
    }

    fn get_module(&mut self) -> &mut Module {
        &mut self.module
    }

    fn get_function(&mut self, name: &str) -> Option<(Function, bool)> {
        match self.module.get_named_function(name) {
            Some(f) => {
                let bb = f.count_basic_blocks();
                Some((f, bb > 0))
            }
            None => None,
        }
    }
}
