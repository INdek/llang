/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/


pub mod codegen;
pub mod context;
pub mod module_provider;
pub mod irbuilder;

pub use module_provider::{SimpleModuleProvider, ModuleProvider};
pub use irbuilder::IRBuilder;
pub use context::Context;



// let mut context = Context::new();
// let mut module_provider = SimpleModuleProvider::new(&mut context, "module_name");

// ast.codegen(&mut context, &mut module_provider).expect("Codegen error");

// //for node in ast.iter() {
// //    println!("Building: {:?}", node);
// //    codegen::build_node(&node, &mut llvm_conf);
// //}

// module_provider.dump();
// println!("-- -- -- -- Done! -- -- -- --");
