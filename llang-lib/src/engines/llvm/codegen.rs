/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use kailua_syntax::ast::{Block, St, Ex};

use context::Context;
use irbuilder::{IRBuilder, IRBuildingResult};
use module_provider::{ModuleProvider, SimpleModuleProvider};

impl<MP: ModuleProvider> IRBuilder<MP> for Block {
    fn codegen(&self, context: &mut Context, module_provider: &mut MP) -> IRBuildingResult {
        self.iter()
            .fold(Err("empty Block".into()), |_, spanned_node| {
                let node = &spanned_node.base;
                Ok(node.codegen(context, module_provider)?)
            })
    }
}

impl<MP: ModuleProvider> IRBuilder<MP> for St {
    fn codegen(&self, context: &mut Context, module_provider: &mut MP) -> IRBuildingResult {
        match *self {
            St::FuncDecl(ref name, ref signature, ref scope, ref block, ref _scope) => {
                println!("FN: {:?} {:?} {:?} {:?}", name, signature, scope, block);
                //let function = match module_provider.get_function() {
                //    Some(f) => {
                //        return Err("fn redef not supported".into());
                //    }
                //    None => {
                //    }
                //};

                Err("Unsuported expression".into())
            }
            St::Return(ref expr) => {
                //TODO: handle multiple returns
                let val = expr[0].base.codegen(context, module_provider)?;
                Ok(context.builder.build_ret(val.into()).into())
            }
            St::Oops => Err("Oops".into()),
            _ => Err("Unsuported Statement".into()),
        }
    }
}

impl<MP: ModuleProvider> IRBuilder<MP> for Ex {
    fn codegen(&self, context: &mut Context, module_provider: &mut MP) -> IRBuildingResult {
        match *self {
            Ex::Num(n) => Ok(context.context.cons(n).into()),
            //&Ex::False => Some(context.cons(true)),
            _ => Err("Unsuported expression".into()),
        }
    }
}



//pub fn build_expr(expr: &Ex, conf: &mut LLVMConfig) -> Option<LLVMValueRef> {
//    match expr {
//        &Ex::Num(n) => Some(conf.context.cons(n)),
//        //&Ex::False => Some(context.cons(true)),
//        _ => None,
//    }
//}
//
//pub fn build_node(node: &St, conf: &mut LLVMConfig) -> Option<LLVMValueRef> {
//    match node {
//        &St::Return(ref expr) => {
//            let val = build_expr(&expr[0].base, conf).unwrap();
//            Some(conf.builder.build_ret(val))
//        }
//        _ => None
//    }
//}

#[cfg(test)]
mod tests {
    use llvm::*;

    fn create_test_env() -> (Context, Module, Builder) {
        let context = Context::new();
        let module = context.module_create_with_name("test_module");
        let builder = context.create_builder();
        (context, module, builder)
    }

    #[test]
    fn build_expr_generates_nums() {
        let (ctx, module, builder) = create_test_env();

    }
}
