/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use llvm::{self, Value, Builder};
use std::collections::HashMap;

pub struct Context<'a> {
    pub context: llvm::Context,
    pub builder: Builder,
    pub named_values: HashMap<String, &'a Value>,
}

impl<'a> Context<'a> {
    pub fn new() -> Context<'a> {
        let context = llvm::Context::global();
        let builder = context.create_builder();
        let named_values = HashMap::new();

        Context {
            context: context,
            builder: builder,
            named_values: named_values,
        }
    }
}