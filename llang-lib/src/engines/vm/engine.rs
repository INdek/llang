/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use Ast;
use engines::Engine;
pub use super::*;


#[derive(Debug, Clone, Default)]
pub struct VmEngine {}

impl Engine for VmEngine {
    type CompiledCode = VmCode;


    fn compile(&self, ast: Ast) -> Self::CompiledCode {
        let result = ast.codegen().expect("test");
        println!("{:?}", result);
        result
    }

    fn run(&self, code: Self::CompiledCode) {
        for inst in code.into_iter() {
            println!("{:?}", inst);
        }
    }
}
