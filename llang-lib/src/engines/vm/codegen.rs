/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use super::*;
use super::OpCode::{self, Ret};
use kailua_syntax::ast::{Block, St, Ex};

pub type CodeBuilderResult = Result<VmCode, String>;

pub trait CodeBuilder {
    fn codegen(&self) -> CodeBuilderResult;
}


impl CodeBuilder for Block {
    fn codegen(&self) -> CodeBuilderResult {
        self.iter()
            .fold(Ok(vec![]), |acc, ref spanned_node| {
                let node = &spanned_node.base;
                let child_code = node.codegen()?;
                let join_res = acc.map(|mut vec| {
                    vec.extend(child_code);
                    vec
                });
                join_res
            })
    }
}

impl CodeBuilder for St {
    fn codegen(&self) -> CodeBuilderResult {
        match *self {
            St::Return(ref expr) => {
                //TODO: handle multiple returns
                //let val = expr[0].base.codegen(context, module_provider)?;
                //Ok(context.builder.build_ret(val.into()).into())
                Ok(vec![
                    Ret{
                        value: 0,
                    }
                ])
            }
            St::Oops => Err("Oops".into()),
            _ => Err("Unsuported Statement".into()),
        }
    }
}


impl CodeBuilder for Ex {
    fn codegen(&self) -> CodeBuilderResult {
        match *self {
            //Ex::Num(n)  | //=> Ok(context.context.cons(n).into()),
            _ => Err("Unsuported Statement".into()),
        }
    }
}
