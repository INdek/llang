/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use Ast;

use std::fmt::Debug;

#[cfg(feature = "llvm_engine")]
pub mod llvm;

#[cfg(feature = "vm_engine")]
pub mod vm;

pub trait Engine {
    type CompiledCode: Debug;

    fn compile(&self, ast: Ast) -> Self::CompiledCode;

    fn run(&self, code: Self::CompiledCode);
}
