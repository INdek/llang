/*
 * Copyright (C) the llang contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

//! A WIP LLVM Based compiler for lua

//#![deny(missing_docs)]
//#![deny(warnings)]
#![doc(test(attr(allow(unused_variables), deny(warnings))))]

#[macro_use]
extern crate error_chain;

#[cfg(feature = "llvm_engine")]
extern crate llvm;
#[cfg(feature = "llvm_engine")]
extern crate llvm_sys;

extern crate kailua_env;
extern crate kailua_syntax;
extern crate kailua_diag;



mod engines;

use engines::Engine;

use kailua_diag::ConsoleReport;
use kailua_syntax::ast::St;
use kailua_env::{Spanned, Source, SourceFile};

use std::path::Path;
use std::rc::Rc;
use std::cell::RefCell;

pub type Ast = Vec<Spanned<Box<St>>>;

error_chain! {
    foreign_links {
        Io(::std::io::Error);
    }
}

pub fn parse(file_name: String) -> Result<Ast> {
    let mut source = Source::new();
    let source2 = Source::new();
    let src_file = SourceFile::from_file(Path::new(&file_name))?;
    let span = source.add(src_file);
    let report = ConsoleReport::new(Rc::new(RefCell::new(source2)));
    let chunk = kailua_syntax::parse_chunk(&source, span, &report).expect("Failed to parse");

    let block = chunk.block.base;

    Ok(block)
}

pub fn run(ast: Ast) {
    let engine = engines::vm::VmEngine::default();
    let code = engine.compile(ast);
    engine.run(code);
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}