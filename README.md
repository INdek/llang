# llang [![travis-badge][]][travis] [![appveyor-badge][]][appveyor] [![coveralls-badge][]][coveralls] [![license-badge][]][license] [![gitter-badge][]][gitter]

Experimental lua compiler 

Current work is being done in [nom-lua](https://github.com/afonso360/nom-lua) a lua 5.3 language parse based on nom

## License

llang is licensed under GPL General Public License 2.0 only

Read [LICENSE][license] for more information.

[travis-badge]: https://img.shields.io/travis/afonso360/llang/master.svg?style=flat-square
[appveyor-badge]: https://img.shields.io/appveyor/ci/afonso360/llang/master.svg?style=flat-square
[coveralls-badge]: https://img.shields.io/coveralls/afonso360/llang/master.svg?style=flat-square
[license-badge]: https://img.shields.io/github/license/afonso360/llang.svg?style=flat-square
[gitter-badge]: https://img.shields.io/gitter/room/afonso360/llang.svg?style=flat-square
[travis]: https://travis-ci.org/afonso360/llang
[appveyor]: https://ci.appveyor.com/project/afonso360/llang
[coveralls]: https://coveralls.io/github/afonso360/llang
[license]: LICENSE
[gitter]: https://gitter.im/llang-llvm
